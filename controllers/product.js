const Product = require("../models/Product")

module.exports.getProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

module.exports.getSpecific = (productId) => {
	return Product.findById(productId).then(result => {
		return result;
	})
}

module.exports.addProduct = (body) => {
	let newProduct = new Product({
		productName: body.productName,
		description: body.description,
		price: body.price
	})
	return newProduct.save().then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.updateProduct = (productId, body) => {
	let updateProduct = {
		productName: body.productName,
		description: body.description,
		price: body.price		
	}
	return Product.findByIdAndUpdate(productId, updateProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.archiveProduct = (productId) => {
	let updatedProduct ={
		isActive: false
	}

	return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}