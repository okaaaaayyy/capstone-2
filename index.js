// set up dependencies
const express = require("express");
const mongoose = require("mongoose");
// import routes
const productRoutes = require("./routes/product")
const userRoutes = require("./routes/user")

// add database conenction
mongoose.connect("mongodb+srv://admin:admin@testdatabase1.9lbsm.mongodb.net/capstone-2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// database confirmation message
mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas."))

// server setup
const app = express()

// middleware that allows our app to receive nested JSON data
app.use(express.json())
app.use(express.urlencoded({
	extended: true
}))

// add imported routes
app.use("/products", productRoutes)
app.use("/users", userRoutes)

const port = 4000

app.listen(process.env.PORT || port, () => {
	console.log(`Server running on port ${port}`)
})