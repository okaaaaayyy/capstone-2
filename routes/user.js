const express = require("express");
const router = express.Router()
const userController = require("../controllers/user");
const auth = require("../auth")

// route for checking if an email exists in our db
router.post("/checkEmail", (req, res) => {
	userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController))
})

// route for user registration
router.post("/register", (req, res) => {
	userController.register(req.body).then(resultFromController => res.send(resultFromController))
})

// route for user login
router.post("/login", (req, res) => {
	userController.login(req.body).then(resultFromController => res.send(resultFromController))
})

module.exports = router;
